-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-11-2017 a las 05:58:46
-- Versión del servidor: 10.1.28-MariaDB
-- Versión de PHP: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyfinalwebviajes`
--
CREATE DATABASE IF NOT EXISTS `proyfinalwebviajes` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `proyfinalwebviajes`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aerolinea`
--

CREATE TABLE `aerolinea` (
  `idaerolinea` int(16) NOT NULL,
  `nombre` varchar(30) CHARACTER SET utf8mb4 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `aerolinea`
--

INSERT INTO `aerolinea` (`idaerolinea`, `nombre`) VALUES
(1, 'Copa Airlines'),
(2, 'American Airlines'),
(3, 'UCA Airlines');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aeropuerto`
--

CREATE TABLE `aeropuerto` (
  `idaeropuerto` int(16) NOT NULL,
  `idciudad` int(16) NOT NULL,
  `nombre` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `aeropuerto`
--

INSERT INTO `aeropuerto` (`idaeropuerto`, `idciudad`, `nombre`) VALUES
(1, 1, 'Augusto C. Sandino'),
(2, 3, 'Bluefilds'),
(3, 4, 'Toncotin'),
(4, 5, 'Guanaja'),
(5, 6, 'El Prat'),
(6, 7, 'Adolfo Suarez');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `avion`
--

CREATE TABLE `avion` (
  `idavion` int(16) NOT NULL,
  `idaerolinea` int(16) NOT NULL,
  `modelo` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `avion`
--

INSERT INTO `avion` (`idavion`, `idaerolinea`, `modelo`) VALUES
(1, 1, 'Boeing 747 - AR1548'),
(2, 1, 'A320 - AD0845'),
(3, 2, 'Boeing 757 - GR1545'),
(4, 2, 'Boeing 747 - ED4551'),
(5, 3, 'Boeing 747 - TE1548');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE `ciudad` (
  `idciudad` int(16) NOT NULL,
  `idpais` int(16) NOT NULL,
  `nombre` varchar(40) CHARACTER SET utf8mb4 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ciudad`
--

INSERT INTO `ciudad` (`idciudad`, `idpais`, `nombre`) VALUES
(1, 1, 'Managua'),
(3, 1, 'Bluefields'),
(4, 2, 'Tegucigalpa'),
(5, 2, 'Guanaja'),
(6, 3, 'Barcelona'),
(7, 3, 'Madrid');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clase`
--

CREATE TABLE `clase` (
  `idclase` int(16) NOT NULL,
  `idvuelo` int(16) NOT NULL,
  `clase` varchar(20) CHARACTER SET utf8mb4 NOT NULL,
  `disponibilidad` int(16) NOT NULL,
  `precio` decimal(20,4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clase`
--

INSERT INTO `clase` (`idclase`, `idvuelo`, `clase`, `disponibilidad`, `precio`) VALUES
(1, 1, 'TURISTA', 40, '560.0000'),
(2, 1, 'EMPRESARIAL', 30, '675.5000'),
(3, 1, 'PRIMERACLASE', 12, '1032.2000'),
(4, 2, 'TURISTA', 50, '740.5000'),
(5, 2, 'EMPRESARIAL', 35, '920.0000'),
(6, 2, 'PRIMERACLASE', 8, '1200.0000'),
(7, 3, 'TURISTA', 80, '520.3000'),
(8, 3, 'EMPRESARIAL', 50, '750.2000'),
(9, 3, 'PRIMERACLASE', 20, '1020.3000'),
(10, 4, 'TURISTA', 40, '420.2000'),
(11, 4, 'EMPRESARIAL', 20, '610.5000'),
(12, 4, 'PRIMERACLASE', 10, '840.2000'),
(13, 5, 'TURISTA', 80, '250.5000'),
(14, 5, 'EMPRESARIAL', 60, '450.5000'),
(15, 5, 'PRIMERACLASE', 30, '850.2000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `idpais` int(16) NOT NULL,
  `nombre` varchar(40) CHARACTER SET utf8mb4 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`idpais`, `nombre`) VALUES
(1, 'Nicaragua'),
(2, 'Honduras'),
(3, 'España'),
(4, 'Mexico');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pasajero`
--

CREATE TABLE `pasajero` (
  `idpasajero` int(16) NOT NULL,
  `idusuario` int(16) NOT NULL,
  `nombre` varchar(40) CHARACTER SET utf8mb4 NOT NULL,
  `apellido` varchar(40) CHARACTER SET utf8mb4 NOT NULL,
  `edad` int(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ticket`
--

CREATE TABLE `ticket` (
  `idticket` int(16) NOT NULL,
  `idpasajero` int(16) NOT NULL,
  `idclase` int(16) NOT NULL,
  `fecha_compra` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idusuario` int(16) NOT NULL,
  `email` varchar(40) NOT NULL,
  `usuario` varchar(40) NOT NULL,
  `contrasenia` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vuelo`
--

CREATE TABLE `vuelo` (
  `idvuelo` int(16) NOT NULL,
  `idavion` int(16) NOT NULL,
  `fechasalida` datetime NOT NULL,
  `fechallegada` datetime NOT NULL,
  `origen` int(16) NOT NULL,
  `destino` int(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `vuelo`
--

INSERT INTO `vuelo` (`idvuelo`, `idavion`, `fechasalida`, `fechallegada`, `origen`, `destino`) VALUES
(1, 1, '2017-11-21 09:30:00', '2017-11-21 13:00:00', 1, 4),
(2, 5, '2017-11-21 08:00:00', '2017-11-21 12:30:00', 1, 5),
(3, 5, '2017-11-21 09:00:00', '2017-11-20 13:10:00', 1, 6),
(4, 2, '2017-11-21 00:00:00', '2017-11-21 06:00:00', 1, 4),
(5, 5, '2017-11-21 07:00:00', '2017-11-20 11:00:00', 1, 3);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aerolinea`
--
ALTER TABLE `aerolinea`
  ADD PRIMARY KEY (`idaerolinea`);

--
-- Indices de la tabla `aeropuerto`
--
ALTER TABLE `aeropuerto`
  ADD PRIMARY KEY (`idaeropuerto`),
  ADD KEY `idciudad` (`idciudad`);

--
-- Indices de la tabla `avion`
--
ALTER TABLE `avion`
  ADD PRIMARY KEY (`idavion`),
  ADD KEY `idaerolinea` (`idaerolinea`);

--
-- Indices de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`idciudad`),
  ADD KEY `idpais` (`idpais`);

--
-- Indices de la tabla `clase`
--
ALTER TABLE `clase`
  ADD PRIMARY KEY (`idclase`),
  ADD KEY `idvuelo` (`idvuelo`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`idpais`);

--
-- Indices de la tabla `pasajero`
--
ALTER TABLE `pasajero`
  ADD PRIMARY KEY (`idpasajero`),
  ADD KEY `idusuario` (`idusuario`);

--
-- Indices de la tabla `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`idticket`),
  ADD KEY `idpasajero` (`idpasajero`),
  ADD KEY `idclase` (`idclase`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idusuario`);

--
-- Indices de la tabla `vuelo`
--
ALTER TABLE `vuelo`
  ADD PRIMARY KEY (`idvuelo`),
  ADD KEY `vuelo_ibfk_1` (`origen`),
  ADD KEY `destino` (`destino`),
  ADD KEY `idavion` (`idavion`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `aerolinea`
--
ALTER TABLE `aerolinea`
  MODIFY `idaerolinea` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `aeropuerto`
--
ALTER TABLE `aeropuerto`
  MODIFY `idaeropuerto` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `avion`
--
ALTER TABLE `avion`
  MODIFY `idavion` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  MODIFY `idciudad` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `clase`
--
ALTER TABLE `clase`
  MODIFY `idclase` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `idpais` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `pasajero`
--
ALTER TABLE `pasajero`
  MODIFY `idpasajero` int(16) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ticket`
--
ALTER TABLE `ticket`
  MODIFY `idticket` int(16) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idusuario` int(16) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `vuelo`
--
ALTER TABLE `vuelo`
  MODIFY `idvuelo` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `aeropuerto`
--
ALTER TABLE `aeropuerto`
  ADD CONSTRAINT `aeropuerto_ibfk_1` FOREIGN KEY (`idciudad`) REFERENCES `ciudad` (`idciudad`);

--
-- Filtros para la tabla `avion`
--
ALTER TABLE `avion`
  ADD CONSTRAINT `avion_ibfk_1` FOREIGN KEY (`idaerolinea`) REFERENCES `aerolinea` (`idaerolinea`);

--
-- Filtros para la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD CONSTRAINT `ciudad_ibfk_1` FOREIGN KEY (`idpais`) REFERENCES `pais` (`idpais`);

--
-- Filtros para la tabla `clase`
--
ALTER TABLE `clase`
  ADD CONSTRAINT `clase_ibfk_1` FOREIGN KEY (`idvuelo`) REFERENCES `vuelo` (`idvuelo`);

--
-- Filtros para la tabla `pasajero`
--
ALTER TABLE `pasajero`
  ADD CONSTRAINT `pasajero_ibfk_1` FOREIGN KEY (`idusuario`) REFERENCES `usuario` (`idusuario`);

--
-- Filtros para la tabla `ticket`
--
ALTER TABLE `ticket`
  ADD CONSTRAINT `ticket_ibfk_1` FOREIGN KEY (`idpasajero`) REFERENCES `pasajero` (`idpasajero`),
  ADD CONSTRAINT `ticket_ibfk_2` FOREIGN KEY (`idclase`) REFERENCES `clase` (`idclase`);

--
-- Filtros para la tabla `vuelo`
--
ALTER TABLE `vuelo`
  ADD CONSTRAINT `vuelo_ibfk_1` FOREIGN KEY (`origen`) REFERENCES `aeropuerto` (`idaeropuerto`),
  ADD CONSTRAINT `vuelo_ibfk_2` FOREIGN KEY (`destino`) REFERENCES `aeropuerto` (`idaeropuerto`),
  ADD CONSTRAINT `vuelo_ibfk_3` FOREIGN KEY (`idavion`) REFERENCES `avion` (`idavion`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php namespace Controllers;

    use Models\Itinerario as Itinerario;

    class itinerarioController
    {
        private $itinerario;

        public function __construct()
        {
            $this->itinerario = new Itinerario();
        }
        public function index()
        {
            $datos = $this->itinerario->listarItinerario();
            return $datos;
        }
    }
    
?>
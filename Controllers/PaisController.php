<?php namespace Controllers;

    use Models\Pais as Pais;

    class paisController
    {
        private $pais;

        public function __construct()
        {
            $this->pais = new Pais();
        }
        public function index()
        {
            $datos = $this->pais->listarPais();
            return $datos;
        }



         public function agregar()
        {
            if(!$_POST)
            {
                $datos = $this->pais->listarPais();
            }
            else
            {
                $this->pais->set("nombre", $_POST['nombrepais']);
                $this->pais->guardarPais();
                header("Location: ". URL . "pais");
            }
        }

        public function editar($idpais)
        {
            if(!$_POST)
            {
                $this->pais->set("idpais", $idpais);
                $datos = $this->pais->verPais();
                return $datos;
            }
            else
            {
                $this->pais->set("idpais", $_POST['idpais']);
                $this->pais->set("nombre", $_POST['nombrepais']);
                $this->pais->editarPais();
                header("Location: ". URL . "pais");
            }
        }

        public function eliminar($idpais)
        {
            $this->pais->set("idpais",$idpais);     
            $datos = $this->pais->eliminarPais();
            header("Location: " . URL . "pais");

        }

    }
    
?>
<?php namespace Controllers;

    use Models\Lugares as Lugares;
    use Models\Itinerario as Itinerario;

    class reservaController
    {
        private $lugares;
        private $itinerario;

        public function __construct()
        {
            $this->lugares = new Lugares();
            $this->itinerario = new Itinerario();
        }
        public function index()
        {
            $datos = $this->lugares->listarLugares();
            return $datos;
        }

        public function paso2()
        {
            if($_GET)
            {
                echo $_GET['origenid'];
                echo $_GET['destinoid'];
                $datos = $this->itinerario->vueloDisponible($_GET['origenid'], $_GET['destinoid'], $_GET['date']);
                return $datos;
            }
        }

        public function paso3()
        {
            return "";
        }

        public function paso4()
        {
            return "";
        }
    }
    
?>
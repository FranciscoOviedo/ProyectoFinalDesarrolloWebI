<?php namespace Controllers;

    use Models\Ciudad as Ciudad;
    use Models\VistaCiudad as VistaCiudad;

    class ciudadController
    {
        private $ciudad;
        private $vistaCiudad;

        public function __construct()
        {
            $this->vistaCiudad = new VistaCiudad();
        }
        public function index()
        {
            $datos = $this->vistaCiudad->listarCiudad();
            return $datos;
        }



         public function agregar()
        {
            if(!$_POST)
            {
                $datos = $this->ciudad->listarCiudad();
            }
            else
            {
                $this->ciudad->set("nombre", $_POST['nombreciudad']);
                $this->ciudad->guardarCiudad();
                header("Location: ". URL . "ciudad");
            }
        }

        public function editar($idciudad)
        {
            if(!$_POST)
            {
                $this->ciudad->set("idciudad", $idciudad);
                $datos = $this->ciudad->verCiudad();
                return $datos;
            }
            else
            {
                $this->ciudad->set("idciudad", $_POST['idciudad']);
                $this->ciudad->set("nombre", $_POST['nombreciudad']);
                $this->ciudad->editarCiudad();
                header("Location: ". URL . "ciudad");
            }
        }

        public function eliminar($idpais)
        {
            $this->ciudad->set("idciudad",$idciudad);     
            $datos = $this->ciudad->eliminarCiudad();
            header("Location: " . URL . "ciudad");

        }

    }
    
?>
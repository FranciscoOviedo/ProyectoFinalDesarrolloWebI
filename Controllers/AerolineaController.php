<?php namespace Controllers;

    use Models\Aerolinea as Aerolinea;

    class aerolineaController
    {
        private $aerolinea;

        public function __construct()
        {
            $this->aerolinea = new Aerolinea();
        }
        public function index()
        {
            $datos = $this->aerolinea->listarAerolinea();
            return $datos;
        }



         public function agregar()
        {
            if(!$_POST)
            {
                $datos = $this->aerolinea->listarAerolinea();
            }
            else
            {
                $this->aerolinea->set("nombre", $_POST['nombreaerolinea']);
                $this->aerolinea->guardarAerolinea();
                header("Location: ". URL . "aerolinea");
            }
        }

        public function editar($idaerolinea)
        {
            if(!$_POST)
            {
                $this->aerolinea->set("idaerolinea", $idaerolinea);
                $datos = $this->aerolinea->verAerolinea();
                return $datos;
            }
            else
            {
                $this->aerolinea->set("idaerolinea", $_POST['idaerolinea']);
                $this->aerolinea->set("nombre", $_POST['nombreaerolinea']);
                $this->aerolinea->editarAerolinea();
                header("Location: ". URL . "aerolinea");
            }
        }

        public function eliminar($idaerolinea)
        {
            $this->aerolinea->set("idaerolinea",$idaerolinea);     
            $datos = $this->aerolinea->eliminarAerolinea();
            header("Location: " . URL . "aerolinea");

        }

    }
    
?>
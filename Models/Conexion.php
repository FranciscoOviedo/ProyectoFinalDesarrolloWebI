<?php namespace Models;

    class Conexion extends \mysqli
    {
        private $con;
        private $datos = array(
            "host" => "localhost",
            "user" => "root",
            "pass" => "",
            "db" => "proyfinalwebviajes"
        );

        public function __construct()
        {
            parent::__construct($this->datos['host'],
            $this->datos['user'], $this->datos['pass'],
            $this->datos['db']);
            $this->set_charset('utf-8');
            /*$this->connection_errno ? die('Error de conexion' . \mysqli_connect_erno()): $m ='CONECTADO';*/
        }

        public function consultaSimple($sql)
        {
            $this->query($sql);
        }

        public function consultaRetorno($sql)
        {
            $datos = $this->query($sql);
            return $datos;
        }
    }
?>
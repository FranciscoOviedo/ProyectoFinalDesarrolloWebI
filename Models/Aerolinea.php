<?php namespace Models;
    class Aerolinea extends Conexion
    {
        //Atributos
        private $idaerolinea;
        private $nombre;

        //Metodos
        public function set($atributo, $contenido)
        {
            $this->$atributo = $contenido;
        }

        public function get($atributo)
        {
            return $this->$atributo;
        }

        //Metodos con Base de Datos
        public function listarAerolinea()
        {
            $sql = "SELECT * FROM aerolinea";
            $datos = $this->consultaRetorno($sql);
            return $datos;
        }

        public function guardarAerolinea()
        {
            $sql = "INSERT INTO aerolinea(nombre) VALUES ('{$this->nombre}')";
            $this->consultaSimple($sql);
        }

        public function view()
        {
            $sql = "SELECT * FROM aerolinea WHERE idaerolinea = '{$this->idaerolinea}'";
            $datos = $this->consultaRetorno($sql);
            $row = \mysqli_fetch_assoc($datos);
            return $row;
        }

        public function editarAerolinea()
        {
            
            $sql = "UPDATE aerolinea set nombre = '{$this->nombre}' WHERE idaerolinea = '{$this->idaerolinea}'";
            
            $this->consultaSimple($sql);
        }
    
        public function verAerolinea()
        {
            $sql = "SELECT * FROM aerolinea where idaerolinea = '{$this->idaerolinea}' ";
            $datos = $this->consultaRetorno($sql);
            $row = \mysqli_fetch_assoc($datos);
            return $row;
    
        }
        public function eliminarAerolinea()
        {
            $sql = "DELETE FROM aerolinea WHERE idaerolinea = '{$this->idaerolinea}'";
            
            $this->consultaSimple($sql);
        }
    }
?>
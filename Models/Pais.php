<?php namespace Models;
    class Pais extends Conexion
    {
        //Atributos
        private $idpais;
        private $nombre;

        //Metodos
        public function set($atributo, $contenido)
        {
            $this->$atributo = $contenido;
        }

        public function get($atributo)
        {
            return $this->$atributo;
        }

        //Metodos con Base de Datos
        public function listarPais()
        {
            $sql = "SELECT * FROM pais";
            $datos = $this->consultaRetorno($sql);
            return $datos;
        }

        public function guardarPais()
        {
            $sql = "INSERT INTO pais (nombre) VALUES ('{$this->nombre}')";
            $this->consultaSimple($sql);
        }

        public function view()
        {
            $sql = "SELECT * FROM pais WHERE idpais = '{$this->idpais}'";
            $datos = $this->consultaRetorno($sql);
            $row = \mysqli_fetch_assoc($datos);
            return $row;
        }

        public function editarPais()
        {
            
            $sql = "UPDATE pais set nombre = '{$this->nombre}' WHERE idpais = '{$this->idpais}'";
            
            $this->consultaSimple($sql);
        }
    
        public function verPais()
        {
            $sql = "SELECT * FROM pais where idpais = '{$this->idpais}' ";
            $datos = $this->consultaRetorno($sql);
            $row = \mysqli_fetch_assoc($datos);
            return $row;
    
        }
        public function eliminarPais()
        {
            $sql = "DELETE FROM pais WHERE idpais = '{$this->idpais}'";
            
            $this->consultaSimple($sql);
        }
    }
?>
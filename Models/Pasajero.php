<?php namespace Models;
    class Pasajero extends Conexion
    {
        //Atributos
        private $idpasajero;
        private $idusuario;
        private $nombre;
        private $apellido;
        private $edad;

        //Metodos
        public function set($atributo, $contenido)
        {
            $this->$atributo = $contenido;
        }

        public function get($atributo)
        {
            return $this->$atributo;
        }

        //Metodos con Base de Datos
        public function listarPasajero()
        {
            $sql = "SELECT * FROM pasajero";
            $datos = $this->consultaRetorno($sql);
            return $datos;
        }

        public function guardarPasajero()
        {
            $sql = "INSERT INTO pasajero(idusuario, nombre, apellido, edad) VALUES ('{$this->idusuario}', '{$this->nombre}', '{$this->apellido}', '{$this->edad}')";
            $this->consultaSimple($sql);
        }

        public function view()
        {
            $sql = "SELECT * FROM pasajero WHERE idpasajero = '{$this->idpasajero}'";
            $datos = $this->consultaRetorno($sql);
            $row = \mysqli_fetch_assoc($datos);
            return $row;
        }
    }
?>
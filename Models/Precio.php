<?php namespace Models;
    class Precio extends Conexion
    {
        //Atributos
        private $idprecio;
        private $idvuelo;
        private $clase;
        private $tipo;
        private $precio;

        //Metodos
        public function set($atributo, $contenido)
        {
            $this->$atributo = $contenido;
        }

        public function get($atributo)
        {
            return $this->$atributo;
        }

        //Metodos con Base de Datos
        public function listarPrecio()
        {
            $sql = "SELECT * FROM precio";
            $datos = $this->consultaRetorno($sql);
            return $datos;
        }

        public function guardarPrecio()
        {
            $sql = "INSERT INTO precio(idvuelo, clase, tipo, precio) VALUES ('{$this->idvuelo}', '{$this->clase}', '{$this->tipo}', '{$this->precio}')";
            $this->consultaSimple($sql);
        }

        public function view()
        {
            $sql = "SELECT * FROM precio WHERE idprecio = '{$this->idprecio}'";
            $datos = $this->consultaRetorno($sql);
            $row = \mysqli_fetch_assoc($datos);
            return $row;
        }
    }
?>
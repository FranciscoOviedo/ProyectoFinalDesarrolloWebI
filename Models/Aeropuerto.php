<?php namespace Models;
    class Aeropuerto extends Conexion
    {
        //Atributos
        private $idaeropuerto;
        private $idciudad;
        private $nombre;

        //Metodos
        public function set($atributo, $contenido)
        {
            $this->$atributo = $contenido;
        }

        public function get($atributo)
        {
            return $this->$atributo;
        }

        //Metodos con Base de Datos
        public function listarAeropuerto()
        {
            $sql = "SELECT * FROM aeropuerto";
            $datos = $this->consultaRetorno($sql);
            return $datos;
        }

        public function guardarUsuario()
        {
            $sql = "INSERT INTO aeropuerto(idciudad, nombre) VALUES ('{$this->idciudad}', '{$this->nombre}')";
            $this->consultaSimple($sql);
        }

        public function view()
        {
            $sql = "SELECT * FROM aeropuerto WHERE idaeropuerto = '{$this->idaeropuerto}'";
            $datos = $this->consultaRetorno($sql);
            $row = \mysqli_fetch_assoc($datos);
            return $row;
        }
    }
?>
<?php namespace Models;
    class Avion extends Conexion
    {
        //Atributos
        private $idavion;
        private $idaerolinea;
        private $modelo;

        //Metodos
        public function set($atributo, $contenido)
        {
            $this->$atributo = $contenido;
        }

        public function get($atributo)
        {
            return $this->$atributo;
        }

        //Metodos con Base de Datos
        public function listarClase()
        {
            $sql = "SELECT * FROM avion";
            $datos = $this->consultaRetorno($sql);
            return $datos;
        }

        public function guardarUsuario()
        {
            $sql = "INSERT INTO avion(idaerolinea, modelo) VALUES ('{$this->idaerolinea}', '{$this->modelo}')";
            $this->consultaSimple($sql);
        }

        public function view()
        {
            $sql = "SELECT * FROM avion WHERE idavion = '{$this->idavion}'";
            $datos = $this->consultaRetorno($sql);
            $row = \mysqli_fetch_assoc($datos);
            return $row;
        }
    }
?>
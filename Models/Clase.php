<?php namespace Models;
    class Clase extends Conexion
    {
        //Atributos
        private $idclase;
        private $idvuelo;
        private $clase;
        private $disponibilidad;
        private $precio;

        //Metodos
        public function set($atributo, $contenido)
        {
            $this->$atributo = $contenido;
        }

        public function get($atributo)
        {
            return $this->$atributo;
        }

        //Metodos con Base de Datos
        public function listarClase()
        {
            $sql = "SELECT * FROM clase";
            $datos = $this->consultaRetorno($sql);
            return $datos;
        }

        public function guardarUsuario()
        {
            $sql = "INSERT INTO clase(idvuelo, clase, disponibilidad, precio) VALUES ('{$this->idvuelo}', '{$this->clase}', '{$this->disponibilidad}', '{$this->precio}')";
            $this->consultaSimple($sql);
        }

        public function view()
        {
            $sql = "SELECT * FROM clase WHERE idclase = '{$this->idclase}'";
            $datos = $this->consultaRetorno($sql);
            $row = \mysqli_fetch_assoc($datos);
            return $row;
        }
    }
?>
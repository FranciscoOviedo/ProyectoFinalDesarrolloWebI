<?php namespace Models;
    class Vuelo extends Conexion
    {
        //Atributos
        private $idvuelo;
        private $idavion;
        private $fechasalida;
        private $fechallegada;
        private $origen;
        private $destino;
        private $conBD;

        public function __contruct()
        {
           $this->conBD = new \Models\Conexion();
        }

        //Metodos
        public function set($atributo, $contenido)
        {
            $this->$atributo = $contenido;
        }

        public function get($atributo)
        {
            return $this->$atributo;
        }

        //Metodos con Base de Datos
        public function listarVuelo()
        {
            $sql = "SELECT * FROM vuelo";
            $datos = $this->consultaRetorno($sql);
            return $datos;
        }

        public function guardarVuelo()
        {
            $sql = "INSERT INTO vuelo(idavion, fechasalida, fechallegada, origen, destino) VALUES ('{$this->idavion}', '{$this->fechasalida}', '{$this->fechallegada}', '{$this->origen}'), '{$this->destino}')";
            $this->consultaSimple($sql);
        }

        public function view()
        {
            $sql = "SELECT * FROM vuelo WHERE idvuelo = '{$this->idvuelo}'";
            $datos = $this->consultaRetorno($sql);
            $row = \mysqli_fetch_assoc($datos);
            return $row;
        }
    }
?>
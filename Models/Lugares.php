<?php namespace Models;
    class Lugares extends Conexion
    {
        //Atributos
        private $idaeropuerto;
        private $lugar;

        //Metodos
        public function set($atributo, $contenido)
        {
            $this->$atributo = $contenido;
        }

        public function get($atributo)
        {
            return $this->$atributo;
        }

        //Metodos con Base de Datos
        public function listarLugares()
        {
            $sql = "SELECT * FROM lugares";
            $datos = $this->consultaRetorno($sql);
            return $datos;
        }
        public function view()
        {
            $sql = "SELECT * FROM lugares WHERE idaeropuerto = '{$this->idaeropuerto}'";
            $datos = $this->consultaRetorno($sql);
            $row = \mysqli_fetch_assoc($datos);
            return $row;
        }
    }
?>
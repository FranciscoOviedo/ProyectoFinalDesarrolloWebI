<?php namespace Models;
    class Ciudad extends Conexion
    {
        //Atributos
        private $idciudad;
        private $idpais;
        private $nombre;

        //Metodos
        public function set($atributo, $contenido)
        {
            $this->$atributo = $contenido;
        }

        public function get($atributo)
        {
            return $this->$atributo;
        }

        //Metodos con Base de Datos
        public function listarCiudad()
        {
            $sql = "SELECT * FROM ciudad";
            $datos = $this->consultaRetorno($sql);
            return $datos;
        }

        public function guardarCiudad()
        {
            $sql = "INSERT INTO ciudad(idciudad, nombre) VALUES ('{$this->idciudad}', '{$this->nombre}')";
            $this->consultaSimple($sql);
        }

        public function view()
        {
            $sql = "SELECT * FROM ciudad WHERE idciudad = '{$this->idciudad}'";
            $datos = $this->consultaRetorno($sql);
            $row = \mysqli_fetch_assoc($datos);
            return $row;
        }



        public function editarCiudad()
        {
            
            $sql = "UPDATE ciudad set nombre = '{$this->nombre}' WHERE idciudad = '{$this->idciudad}'";
            
            $this->consultaSimple($sql);
        }
    
        public function verCiudad()
        {
            $sql = "SELECT * FROM ciudad where idciudad = '{$this->idciudad}' ";
            $datos = $this->consultaRetorno($sql);
            $row = \mysqli_fetch_assoc($datos);
            return $row;
    
        }
        public function eliminarPais()
        {
            $sql = "DELETE FROM ciudad WHERE idciudad = '{$this->idciudad}'";
            
            $this->consultaSimple($sql);
        }
    }
?>
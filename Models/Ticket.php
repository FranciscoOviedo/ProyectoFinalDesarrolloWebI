<?php namespace Models;
    class Ticket extends Conexion
    {
        //Atributos
        private $idticket;
        private $idpasajero;
        private $idprecio;
        private $fecha_compra;

        //Metodos
        public function set($atributo, $contenido)
        {
            $this->$atributo = $contenido;
        }

        public function get($atributo)
        {
            return $this->$atributo;
        }

        //Metodos con Base de Datos
        public function listarTicket()
        {
            $sql = "SELECT * FROM ticket";
            $datos = $this->consultaRetorno($sql);
            return $datos;
        }

        public function guardarTicket()
        {
            $sql = "INSERT INTO ticket(idpasajero, idprecio, fecha_compra) VALUES ('{$this->idpasajero}', '{$this->idprecio}', '{$this->fecha_compra}')";
            $this->consultaSimple($sql);
        }

        public function view()
        {
            $sql = "SELECT * FROM ticket WHERE idticket = '{$this->idticket}'";
            $datos = $this->consultaRetorno($sql);
            $row = \mysqli_fetch_assoc($datos);
            return $row;
        }
    }
?>
<?php namespace Models;
    class Usuario extends Conexion
    {
        //Atributos
        private $idusuario;
        private $email;
        private $usuario;
        private $contrasenia;

        //Metodos
        public function set($atributo, $contenido)
        {
            $this->$atributo = $contenido;
        }

        public function get($atributo)
        {
            return $this->$atributo;
        }

        //Metodos con Base de Datos
        public function listarUsuario()
        {
            $sql = "SELECT * FROM usuario";
            $datos = $this->consultaRetorno($sql);
            return $datos;
        }

        public function guardarUsuario()
        {
            $sql = "INSERT INTO usuario(email, usuario, contrasenia) VALUES ('{$this->email}', '{$this->usuario}', '{$this->contrasenia}')";
            $this->consultaSimple($sql);
        }

        public function view()
        {
            $sql = "SELECT * FROM usuario WHERE idusuario = '{$this->idusuario}'";
            $datos = $this->consultaRetorno($sql);
            $row = \mysqli_fetch_assoc($datos);
            return $row;
        }
    }
?>
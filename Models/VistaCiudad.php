<?php namespace Models;
    class VistaCiudad extends Conexion
    {
        //Atributos
        private $idciudad;
        private $pais;
        private $nombre;

        //Metodos
        public function set($atributo, $contenido)
        {
            $this->$atributo = $contenido;
        }

        public function get($atributo)
        {
            return $this->$atributo;
        }

        //Metodos con Base de Datos
        public function listarCiudad()
        {
            $sql = "SELECT * FROM vistaciudad";
            $datos = $this->consultaRetorno($sql);
            return $datos;
        }


        public function view()
        {
            $sql = "SELECT * FROM vistaciudad WHERE idciudad = '{$this->idciudad}'";
            $datos = $this->consultaRetorno($sql);
            $row = \mysqli_fetch_assoc($datos);
            return $row;
        }


    
        public function verCiudad()
        {
            $sql = "SELECT * FROM ciudad where idciudad = '{$this->idciudad}' ";
            $datos = $this->consultaRetorno($sql);
            $row = \mysqli_fetch_assoc($datos);
            return $row;
    
        }
    }
?>
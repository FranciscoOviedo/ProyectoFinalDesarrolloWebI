create view lugares AS
select aeropuerto.idaeropuerto, CONCAT(aeropuerto.nombre, ', ', ciudad.nombre, ', ', pais.nombre) as 'lugar' from aeropuerto
inner join ciudad on aeropuerto.idciudad = ciudad.idciudad
inner join pais on ciudad.idpais = pais.idpais;
create view vistaciudad as
select ciudad.idciudad, ciudad.nombre, pais.nombre as 'pais' from ciudad
inner join pais on pais.idpais = ciudad.idpais;
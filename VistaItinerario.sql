create view itinerario as
select vuelo.fechasalida, vuelo.fechallegada, CONCAT(aeroorigen.nombre, ', ', ciudorigen.nombre, ', ', paisorigen.nombre) as 'origen', aeroorigen.idaeropuerto as 'origenid', CONCAT(aerodestino.nombre, ', ', ciuddestino.nombre, ', ', paisdestino.nombre) as 'destino', aerodestino.idaeropuerto as 'destinoid', avion.modelo, aerolinea.nombre as 'aerolinea' from vuelo 
inner join aeropuerto as aeroorigen on vuelo.origen = aeroorigen.idaeropuerto
inner join aeropuerto as aerodestino on vuelo.destino = aerodestino.idaeropuerto
inner join ciudad as ciudorigen on aeroorigen.idciudad = ciudorigen.idciudad
inner join ciudad as ciuddestino on aerodestino.idciudad = ciuddestino.idciudad
inner join pais as paisorigen on ciudorigen.idpais = paisorigen.idpais
inner join pais as paisdestino on ciuddestino.idpais = paisdestino.idpais
inner join avion on avion.idavion = vuelo.idavion
inner join aerolinea on aerolinea.idaerolinea = avion.idaerolinea
where vuelo.fechasalida > CURRENT_TIMESTAMP;
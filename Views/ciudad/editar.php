<div class="form-holder">
  <h1 class="form-item">Editar ciudad <?php echo $datos['nombre']; ?> </h1>
  <form class="form-horizontal" action="" method="POST">
    <div class="form-item">
        <label class="control-label">Nombre de la ciudad:</label>
        <input class="form-control" value="<?php echo $datos['nombre']; ?>" type="text" name="nombreciudad" required/>
    </div>
    <input value="<?php echo $datos['idciudad']; ?>" name="idciudad" type="hidden" required />
    <div>
        <button type="submit" class="btn">Editar</button>
        <button type="reset" class="btn">Cancelar</button>
    </div>
  </form>
</div>
<div class="form-holder">
    <h1 class="form-item">Agregar una nueva ciudad</h1>
    <form class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
        <div class="form-item">
            <label for="inputDscCiudad" class="control-label">Nombre de la ciudad:</label>
            <input class="form-control" name="nombreciudad" type="text" required>
        </div>
        <div>
                <button type="submit" class="btn">Registrar</button>
            <button type="reset" class="btn">Borrar</button>
        </div>
    </form>
</div>
<div class="slideshow">
    <ul class="slider">
        <li>
            <img src="<?php echo URL; ?>Views/template/img/1.jpg" alt="">
        </li>
        <li>
            <img src="<?php echo URL; ?>Views/template/img/2.jpeg" alt="">
        </li>
        <li>
            <img src="<?php echo URL; ?>Views/template/img/3.jpg" alt="">
        </li>
        <li>
            <img src="<?php echo URL; ?>Views/template/img/4.jpeg" alt="">
        </li>
    </ul>

    <div class="left">
        <span class="fa fa-chevron-left"></span>
    </div>

    <div class="right">
        <span class="fa fa-chevron-right"></span>
    </div>

</div>
<div class="post">
    <img class="post-img" src="<?php echo URL; ?>Views/template/img/aviones1.jpg" alt="avi">
    <div class="post-body">
        <h1 class="post-title">Primer Post sobre la Aerolinea UCA Airlines </h1>
        <p class="post-parr">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris in eros et sapien consectetur imperdiet porttitor vel erat. Nam placerat sapien at est laoreet, vel ultricies tellus porta. Integer posuere purus at euismod facilisis. In tristique augue sodales vestibulum porttitor. Cras libero metus, tristique in libero nec, vehicula cursus lacus. Nam cursus pulvinar odio vitae rhoncus. Donec eros diam, faucibus et rhoncus a, placerat at turpis. Proin maximus est dolor. Integer leo quam, hendrerit sit amet odio tristique, posuere bibendum diam. Aliquam porta mollis ipsum eu varius. Nam euismod massa ut ex ultrices, eu faucibus enim dignissim. In ultrices, tortor eu dapibus accumsan, tortor nisi eleifend orci, vel consequat est est sed augue. Donec volutpat hendrerit magna, sit amet ultricies ex pharetra vulputate. Vestibulum dapibus velit eu cursus iaculis. Etiam vitae elit sed magna pretium vulputate et quis leo.</p>
    </div>
</div>

<script>
    var menuitem = document.getElementById("menu_1")
    menuitem.classList.add("main-menu__item__selected")
</script>
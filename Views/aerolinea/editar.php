<div class="form-holder">
  <h1 class="form-item">Editar aerolinea <?php echo $datos['nombre']; ?> </h1>
  <form class="form-horizontal" action="" method="POST">
    <div class="form-item">
        <label class="control-label">Nombre de la  aerolinea:</label>
        <input class="form-control" value="<?php echo $datos['nombre']; ?>" type="text" name="nombreaerolinea" required/>
    </div>
    <input value="<?php echo $datos['idaerolinea']; ?>" name="idaerolinea" type="hidden" required />
    <div>
        <button type="submit" class="btn">Editar</button>
        <button type="reset" class="btn">Cancelar</button>
    </div>
  </form>
</div>
<div class="form-holder">
    <img class="indicador" src="<?php echo URL; ?>Views/template/img/indicador2.png" alt="Paso 2">
    <h1 class="form-title">Opciones de Vuelos</h1>
    <table>
        <tr>
            <th>Sale</th>
            <th>Llega</th>
            <th>Precio Individual</th>
            <th>Precio Total</th>
            <th>Avion</th>
            <th></th>
        </tr>
        <?php while($row = mysqli_fetch_array($datos)){ ?>
            <tr>
                <td> <?php echo $row['fechasalida']; ?> </td>
                <td> <?php echo $row['fechallegada']; ?> </td>
                <td>120.00$</td>
                <td>540.00$</td>
                <td> <?php echo $row['modelo']; ?> </td>
                <td><button>Seleccionar</button></td>
            </tr>
        <?php } ?>
    </table>
</div>
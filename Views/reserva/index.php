<div class="form-holder">
    <img class="indicador" src="<?php echo URL; ?>Views/template/img/indicador1.png" alt="Paso 1">
    <h1 class="form-title">Reserva de Vuelo</h1>
    <form method="GET" action="<?php echo URL; ?>reserva/paso2">
        <div class="form-item">
            <label for="origenid">Origen:</label>
            <select name="origenid">
                <?php while($row = mysqli_fetch_array($datos)){ ?>
                    <option value="<?php echo $row['idaeropuerto']?>"><?php echo $row['lugar']?></option>
                <?php } mysqli_data_seek($datos,0);?>
            </select>
        </div>
        <div class="form-item">
            <label for="destinoid">Destino:</label>
            <select name="destinoid">
                <?php while($row = mysqli_fetch_array($datos)){ ?>
                    <option value="<?php echo $row['idaeropuerto']?>"><?php echo $row['lugar']?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-item">
            <label for="date">Fecha:</label>
            <input name="date" type="date" value="2017-01-01">
        </div>
        <div class="form-item">
            <label for="npasajeros">Pasajeros:</label>
            <input id="npasajeros" type="number">
        </div>
        <div class="form-item">
            <label for="clase">Clase:</label>
            <select id="clase">
                <option>Turista</option>
                <option>Empresarial</option>
                <option>Primera Clase</option>
            </select>
        </div>
        <!--button class="btn">Buscar</button-->
        <button type="submit" class="btn" value="buscar"/>Buscar<button></button>
    </form>
    
    <!--button class="btn">Buscar</button-->
</div>

<script>
    var menuitem = document.getElementById("menu_3")
    menuitem.classList.add("main-menu__item__selected")
</script>
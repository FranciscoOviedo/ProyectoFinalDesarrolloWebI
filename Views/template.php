<?php namespace Views;

    $template = new Template();
    
class Template
{
    public function __construct()
    {
?>

        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <link rel="stylesheet" href="<?php echo URL; ?>Views/template/css/normalize.css">
            <link rel="stylesheet" href="<?php echo URL; ?>Views/template/css/styles.css">
            <link rel="stylesheet" href="<?php echo URL; ?>Views/template/css/font-awesome.css">
            <script src="<?php echo URL; ?>Views/template/js/jquery-3.1.0.min.js"></script>
            <script src="<?php echo URL; ?>Views/template/js/main.js"></script>
            <title>Reserva de vuelos UCA</title>
        </head>
        <body>
            <header>
                <div class="logo-header">
                    <img class="logo-img" src="<?php echo URL; ?>Views/template/img/logo.png" alt="logo">
                </div>         
                <div class="navigator-header">
                    <nav class="navigator-menu">
                        <ul class="main-menu">
                            <li id="menu_1" class="main-menu__item"><a href="inicio" class="main-menu__link">Inicio</a></li>
                            <li id="menu_2" class="main-menu__item"><a href="itinerario" class="main-menu__link">Vuelos</a>
                            <ul  class=" main-menu main-menu--child">
                            <li id="submenu_1" class="main-menu__item main-menu__item--child"><a href="pais" class="main-menu__link">Pais</a></li>
                            <li id="submenu_2" class="main-menu__item main-menu__item--child"><a href="aerolinea"class="main-menu__link">Aerolinea</a></li>
                         </ul>                            
                            </li>
                            <li id="menu_3" class="main-menu__item"><a href="reserva" class="main-menu__link">Reservar</a></li>
                            <li id="menu_4" class="main-menu__item"><a href="login_registro" class="main-menu__link">Login/Registrarse</a></li>     
                            
                        </ul>
                    </nav>               
                </div>     
            </header>
            <div id="content">

<?php
    }

    public function __destruct()
    {

?>
            </div>
        </body>
        <footer>
            <p id="f1">Aerolinea © 2017 </p> <p id="f2">Pagina hecha por estudiantes de la UCA</p> 
        </footer>
        </html>  

<?php
    }
}

?>
<div class="form-holder">
  <h1 class="form-item">Editar pais <?php echo $datos['nombre']; ?> </h1>
  <form class="form-horizontal" action="" method="POST">
    <div class="form-item">
        <label class="control-label">Nombre del Pais:</label>
        <input class="form-control" value="<?php echo $datos['nombre']; ?>" type="text" name="nombrepais" required/>
    </div>
    <input value="<?php echo $datos['idpais']; ?>" name="idpais" type="hidden" required />
    <div>
        <button type="submit" class="btn">Editar</button>
        <button type="reset" class="btn">Cancelar</button>
    </div>
  </form>
</div>